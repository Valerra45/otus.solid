﻿using Otus.SOLID.Core.Abstraction;

namespace Otus.SOLID.Application
{
    public class Launcher : ILauncher
    {
        public Launcher(IMenu menu,
            IGame game)
        {
            Menu = menu;
            Game = game;
        }

        private IMenu Menu { get; set; }
        private IGame Game { get; set; }

        public void Start()
        {
            Menu.Show(Game);
        }
    }
}
