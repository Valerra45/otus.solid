﻿using Otus.SOLID.Core.Abstraction;
using System;

namespace Otus.SOLID.Application
{
    public class Generator : IGenerator
    {
        public int Generate(int min, int max)
        {
            return new Random().Next(min, max);
        }
    }
}
