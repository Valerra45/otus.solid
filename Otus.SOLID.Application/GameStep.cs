﻿using Otus.SOLID.Core.Abstraction;
using Otus.SOLID.Core.Enum;
using System;

namespace Otus.SOLID.Application
{
    public class GameStep : IGameStep
    {
        public GameStesResult Do(IGame game)
        {
            Console.Clear();

            Console.Write($"Please enter a number from {game.Settings.MinValue} to {game.Settings.MaxValue}: ");

            game.Number = int.Parse(Console.ReadLine());

            return game.CompareNumbers.Comparer(game.Number, game.GuessNumber);
        }
    }
}
