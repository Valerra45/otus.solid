﻿using Otus.SOLID.Core.Abstraction;
using Otus.SOLID.Core.Models;
using System.Threading;

namespace Otus.SOLID.Application
{
    public class GuessNumberGame : IGame
    {
        private IGameStep _gameStep;
        private IMessage _winMessage,
            _lossMessage,
            _moreMessage,
            _lessMessage;

        public GuessNumberGame(ISettings settings,
            IGenerator generator,
            ICompareNumbers compareNumbers)
        {
            Settings = settings;
            Generator = generator;
            CompareNumbers = compareNumbers;

            _gameStep = new GameStep();

            _winMessage = new WinMessage();
            _lossMessage = new LoseMessage();
            _moreMessage = new MoreMessage();
            _lessMessage = new LessMessage();
        }

        public ISettings Settings { get; set; }

        public IGenerator Generator { get; set; }

        public ICompareNumbers CompareNumbers { get; set; }

        public int Number { get; set; }

        public int GuessNumber { get; set; }

        public void Run()
        {
            GuessNumber = Generator.Generate(Settings.MinValue, Settings.MaxValue);

            for (int i = 0; i < Settings.Attempts; i++)
            {
                switch (_gameStep.Do(this))
                {
                    case Core.Enum.GameStesResult.Win:

                        _winMessage.Show(Number);

                        return;
                    case Core.Enum.GameStesResult.More:

                        _moreMessage.Show(Number);
                        Thread.Sleep(3000);
                        break;
                    case Core.Enum.GameStesResult.Less:

                        _lessMessage.Show(Number); ;
                        Thread.Sleep(3000);
                        break;
                }
            }

            _lossMessage.Show(GuessNumber);
        }
    }
}