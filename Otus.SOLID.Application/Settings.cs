﻿using Otus.SOLID.Core.Abstraction;
using System;

namespace Otus.SOLID.Application
{
    public class Settings : ISettings
    {
        public int MinValue { get; set; } = 1;
        public int MaxValue { get; set; } = 10;
        public int Attempts { get; set; } = 5;

        public void Show()
        {
            Console.WriteLine("Game settings:");
            Console.WriteLine($"Min value - {MinValue}");
            Console.WriteLine($"Max value - {MaxValue}");
            Console.WriteLine($"Attempts - {Attempts}");
        }

        public void Update()
        {
            Console.Clear();
            Console.Write("Enter new min value:");
            MinValue = int.Parse(Console.ReadLine());

            Console.Write("Enter new max value:");
            MaxValue = int.Parse(Console.ReadLine());

            Console.Write("Enter attemps:");
            Attempts = int.Parse(Console.ReadLine());
        }
    }
}
