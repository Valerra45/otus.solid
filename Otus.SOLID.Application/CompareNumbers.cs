﻿using Otus.SOLID.Core.Abstraction;
using Otus.SOLID.Core.Enum;

namespace Otus.SOLID.Application
{
    public class CompareNumbers : ICompareNumbers
    {
        public GameStesResult Comparer(int a, int b)
        {
            var result = a.CompareTo(b);

            if (result < 0)
            {
                return GameStesResult.Less;
            }

            if (result > 0)
            {
                return GameStesResult.More;
            }

            return GameStesResult.Win;
        }
    }
}
