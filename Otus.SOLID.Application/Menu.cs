﻿using Otus.SOLID.Core.Abstraction;
using Otus.SOLID.Core.Enum;
using System;

namespace Otus.SOLID.Application
{
    public class Menu : IMenu
    {
        private static int _arrow = 0;

        private static string[] _menu =
        {
            "Start game",
            "Settings",
            "Exit"
        };

        public void Show(IGame game)
        {
            while (true)
            {
                Console.Clear();

                if (_arrow < (int)MenuItems.Game)
                {
                    _arrow = (int)MenuItems.Exit;
                }
                else if (_arrow > (int)MenuItems.Exit)
                {
                    _arrow = (int)MenuItems.Game;
                }

                for (int i = 0; i < _menu.Length; i++)
                {
                    Console.WriteLine($"{(_arrow == i ? ">>" : "  ")} {_menu[i]}");
                }

                ConsoleKey key = Console.ReadKey().Key;

                if (key == ConsoleKey.UpArrow)
                {
                    _arrow--;
                }
                else if (key == ConsoleKey.DownArrow)
                {
                    _arrow++;
                }
                else if (key == ConsoleKey.Enter)
                {
                    switch ((MenuItems)_arrow)
                    {
                        case MenuItems.Game:
                            Console.Clear();

                            game.Run();

                            Console.WriteLine();
                            Console.WriteLine("Press any key to continue...");
                            Console.ReadKey();

                            break;
                        case MenuItems.Settings:
                            Console.Clear();

                            game.Settings.Show();

                            Console.Write("Change settings? (y/any):");
                            key = Console.ReadKey().Key;

                            if (key == ConsoleKey.Y)
                            {
                                game.Settings.Update();
                            }

                            break;
                        case MenuItems.Exit:
                            Console.Clear();

                            return;
                    }
                }
            }
        }
    }
}