﻿using Otus.SOLID.Core.Abstraction;
using System;

namespace Otus.SOLID.Core.Models
{
    public class LessMessage : IMessage
    {
        public void Show(int value)
        {
            Console.Clear();

            Console.WriteLine($"Your number {value} is less");
        }
    }
}
