﻿using Otus.SOLID.Core.Abstraction;
using System;

namespace Otus.SOLID.Core.Models
{
    public class LoseMessage : IMessage
    {
        public void Show(int value)
        {
            Console.Clear();

            Console.WriteLine($"Unfortunately, you have not guessed the hidden number - {value}");
        }
    }
}
