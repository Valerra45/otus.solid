﻿using Otus.SOLID.Core.Abstraction;
using System;

namespace Otus.SOLID.Core.Models
{
    public class WinMessage : IMessage
    {
        public void Show(int value)
        {
            Console.Clear();

            Console.WriteLine($"Congratulations! You guessed the number - {value}");
        }
    }
}
