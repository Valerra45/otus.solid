﻿namespace Otus.SOLID.Core.Abstraction
{
    public interface IMessage
    {
        void Show(int value);
    }
}
