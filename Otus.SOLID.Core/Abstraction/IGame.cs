﻿namespace Otus.SOLID.Core.Abstraction
{
    public interface IGame
    {
        public ISettings Settings { get; set; }

        public IGenerator Generator { get; set; }

        public ICompareNumbers CompareNumbers { get; set; }

        public int Number { get; set; }

        public int GuessNumber { get; set; }

        void Run();
    }
}
