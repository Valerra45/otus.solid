﻿namespace Otus.SOLID.Core.Abstraction
{
    public interface ISettingsShow
    {
        void Show();
    }
}
