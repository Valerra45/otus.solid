﻿namespace Otus.SOLID.Core.Abstraction
{
    public interface IMenu
    {
        void Show(IGame game);
    }
}
