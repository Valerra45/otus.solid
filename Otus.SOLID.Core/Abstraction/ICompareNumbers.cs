﻿using Otus.SOLID.Core.Enum;

namespace Otus.SOLID.Core.Abstraction
{
    public interface ICompareNumbers
    {
        GameStesResult Comparer(int a, int b);
    }
}
