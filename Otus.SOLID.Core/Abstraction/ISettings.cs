﻿namespace Otus.SOLID.Core.Abstraction
{
    public interface ISettings : ISettingsShow, ISettingsUpdate
    {
        public int MinValue { get; set; }

        public int MaxValue { get; set; }

        public int Attempts { get; set; }
    }
}
