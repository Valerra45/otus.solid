﻿namespace Otus.SOLID.Core.Abstraction
{
    public interface ISettingsUpdate
    {
        void Update();
    }
}
