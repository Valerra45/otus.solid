﻿using Otus.SOLID.Core.Enum;

namespace Otus.SOLID.Core.Abstraction
{
    public interface IGameStep
    {
        GameStesResult Do(IGame game);
    }
}
