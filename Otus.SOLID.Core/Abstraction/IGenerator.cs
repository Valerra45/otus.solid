﻿namespace Otus.SOLID.Core.Abstraction
{
    public interface IGenerator
    {
        int Generate(int min, int max);
    }
}
