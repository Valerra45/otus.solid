﻿namespace Otus.SOLID.Core.Abstraction
{
    public interface ILauncher
    {
        void Start();
    }
}
