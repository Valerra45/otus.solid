﻿using Otus.SOLID.Application;

namespace Otus.SOLID.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            Settings settings = new();

            Generator generator = new();

            CompareNumbers compareNumbers = new();

            GuessNumberGame game = new GuessNumberGame(settings, generator, compareNumbers);

            Menu menu = new();

            new Launcher(menu, game).Start();
        }
    }
}
